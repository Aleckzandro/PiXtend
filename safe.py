#!/usr/bin/env python
# coding=utf-8

# This file is part of the PiXtend(R) Project.
#
# For more information about PiXtend(R) and this program,
# see <http://www.pixtend.de> or <http://www.pixtend.com>
#
# Copyright (C) 2017 Robin Turner
# Qube Solutions UG (haftungsbeschränkt), Arbachtalstr. 6
# 72800 Eningen, Germany
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
# Import Pixtend V2 -S- class
from pixtendv2s import PiXtendV2S
import paho.mqtt.client as mqtt
import time, os, urlparse, sys, re

# -----------------------------------------------------------------
# Print Art and Slogan
# -----------------------------------------------------------------
print("")
print("    ____  _ _  ____                 __   _    _____        _____")
print("   / __ \\(_) |/ / /____  ____  ____/ /  | |  / /__ \\      / ___/")
print("  / /_/ / /|   / __/ _ \\/ __ \\/ __  /   | | / /__/ / ____ \\__ \\ ")
print(" / ____/ //   / /_/  __/ / / / /_/ /    | |/ // __/ /___/__ / / ")
print("/_/   /_//_/|_\\__/\\___/_/ /_/\\__,_/     |___//____/     /____/  ")
print("")

# -----------------------------------------------------------------
# Create instance - SPI communication starts automatically
# -----------------------------------------------------------------
p = PiXtendV2S()

# -----------------------------------------------------
# Main Program
# -----------------------------------------------------
if p is not None:
    print("Running Main Program - Hit Ctrl + C to exit")
    # Set some variables needed in the main loop
    cycle_counter = 0
    water_lvl = 0
    pump = False
    ball_valve = False
    min = 0
    max = 10

    while True:
        try:
                cycle_counter += 1

		#The container gets constantly filled up or emptied if the min or max value is reached
		if water_lvl == max:
			pump = False
			p.digital_out0 = False

			ball_valve = True
			p.digital_out1 = True

		elif water_lvl == min:
			pump = True
			p.digital_out0 = True

			ball_valve = False
			p.digital_out1 = False

		#Set the Outputs
		if pump == True:
			water_lvl += 1
		elif ball_valve  == True:
			water_lvl -= 1
		else:
			print ("error")

                # Build text with values from all digital outputs und relays
                str_text = "Cycle No.: {0}\n".format(cycle_counter)
                str_text += "Digital Input 0:  {0}\n".format(p.digital_in0)
                str_text += "Digital Input 1:  {0}\n".format(p.digital_in1)
                str_text += "Digital Input 2:  {0}\n".format(p.digital_in2)
                str_text += "Digital Input 3:  {0}\n".format(p.digital_in3)
                str_text += "Digital Input 4:  {0}\n".format(p.digital_in4)
                str_text += "Digital Input 5:  {0}\n".format(p.digital_in5)
                str_text += "Digital Input 6:  {0}\n".format(p.digital_in6)
                str_text += "Digital Input 7:  {0}\n".format(p.digital_in7)
                str_text += "Relay 0:          {0}\n".format(p.relay0)
                str_text += "Relay 1:          {0}\n".format(p.relay1)
                str_text += "Relay 2:          {0}\n".format(p.relay2)
                str_text += "Relay 3:          {0}\n".format(p.relay3)
		str_text += "Pump:             {0}\n".format(p.digital_out0)
		str_text += "Ball Valve:       {0}\n".format(p.digital_out1)
		str_text += "Digital Output 2: {0}\n".format(p.digital_out2)
		str_text += "Digital Output 3: {0}\n".format(p.digital_out3)
		str_text += "Wasserfüllstand:  {0}\n".format(water_lvl)

                # Print text to console
                print(str_text, end="\r")

                # Reset cursor
                for i in range(0, 18, 1):
                    sys.stdout.write("\x1b[A") 


                # Wait 2sec or 2000ms
                time.sleep(2)
                # Overwrite old text on screen
                sys.stdout.write(re.sub(r"[^\s]", " ", str_text))
                # Reset cursor
                for i in range(0, 18, 1):
                    sys.stdout.write("\x1b[A")

        except KeyboardInterrupt:
            # Keyboard interrupt caught, Ctrl + C, now clean up and leave program
            for i in range(0, 19, 1):
                print("")
            p.digital_out0 = p.OFF
            p.digital_out1 = p.OFF
            p.digital_out2 = p.OFF
            p.digital_out3 = p.OFF
            p.relay0 = p.OFF
            p.relay1 = p.OFF
            p.relay2 = p.OFF
            p.relay3 = p.OFF
            # We must call the close() function to end the communication with the
            # PiXtend V2 -S- microcontroller, this way no error is thrown and we
            # can start this program again right away.
            p.close()
            # We have to wait for the communication to end
            time.sleep(2)
            p = None
            break
else:
    p = None
    # If there was an error creating the PiXtend V2 -S- instance, leave the program.
    print("")
    print("There was a problem creating the Pixtend V2 -S- instance. Quitting.")
    print("")
